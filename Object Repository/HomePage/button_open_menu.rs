<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_open_menu</name>
   <tag></tag>
   <elementGuidId>9ae7bbde-dedd-4607-806c-8f1bad06bf97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-burger-menu-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='react-burger-menu-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ff1c8c86-1660-4eb2-9957-6faa2736096d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-burger-menu-btn</value>
      <webElementGuid>a87341ce-f1b4-4639-b577-1ae14c8915cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Open Menu</value>
      <webElementGuid>b8c1e2ed-53cb-460d-b37c-9aef879b74ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-burger-menu-btn&quot;)</value>
      <webElementGuid>8bb8e791-ae72-442c-88a9-971ff28380e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='react-burger-menu-btn']</value>
      <webElementGuid>fc6edd17-aae6-4843-8e61-207c51ea5132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='menu_button_container']/div/div/div/button</value>
      <webElementGuid>cecf76e0-ae12-4098-ac2a-d41f1843a5aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/preceding::button[1]</value>
      <webElementGuid>5b26090a-a3a7-452f-85be-16ac8c9a6736</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Menu'])[1]/preceding::button[1]</value>
      <webElementGuid>433c1fc7-cc66-489a-9e58-b6173f0b8385</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Open Menu']/parent::*</value>
      <webElementGuid>3632c98a-c303-460d-9181-888964f221ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>3ea7dd2a-201f-47fa-8ebe-0fcb92ad6396</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'react-burger-menu-btn' and (text() = 'Open Menu' or . = 'Open Menu')]</value>
      <webElementGuid>fc791114-b308-451e-a378-133ecbe9f32b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
