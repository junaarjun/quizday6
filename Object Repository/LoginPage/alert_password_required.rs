<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_password_required</name>
   <tag></tag>
   <elementGuidId>83310849-45f5-4ec7-ba30-7135e1c08ffa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.error-message-container.error</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='login_button_container']/div/form/div[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'error-message-container error' and (text() = 'Epic sadface: Password is required' or . = 'Epic sadface: Password is required')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>93f964db-ba65-437f-8799-e3f485e4c7c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error-message-container error</value>
      <webElementGuid>0e81d2ef-c427-418e-901e-3b118487acbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Epic sadface: Password is required</value>
      <webElementGuid>2f166577-c342-4b0b-81c7-ed8a768fa262</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_button_container&quot;)/div[@class=&quot;login-box&quot;]/form[1]/div[@class=&quot;error-message-container error&quot;]</value>
      <webElementGuid>1a9d6531-38cc-426b-bc34-2096209fc2ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/div[3]</value>
      <webElementGuid>43790a59-1af8-4a56-aa85-c2922d400a09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>85493d4f-0a91-412c-b26d-235d116ff45e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Epic sadface: Username is required' or . = 'Epic sadface: Username is required')]</value>
      <webElementGuid>4a017177-4e01-406c-8420-3ea1546978e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
