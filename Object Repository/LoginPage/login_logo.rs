<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login_logo</name>
   <tag></tag>
   <elementGuidId>e805b426-8973-4de1-9de2-b1d74bf58573</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.login_logo</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>89c1c9ef-c895-4cac-b8d5-e6f9f8325b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login_logo</value>
      <webElementGuid>6fe52f7b-82bd-4182-98fb-71327a2e63a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;login_logo&quot;]</value>
      <webElementGuid>f8a1f36b-83e9-4832-8791-b341ffb5eeb3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div</value>
      <webElementGuid>5bfd4504-dc6f-4178-9375-32276602f644</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
      <webElementGuid>d1cb1c37-d499-4e3f-b7b0-9ac72d1d3019</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
