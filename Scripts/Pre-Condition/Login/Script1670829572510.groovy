import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('LoginPage/login_logo'), 3)

WebUI.setText(findTestObject('LoginPage/input_username'), findTestData('credential (1)').getValue(1, 6))

WebUI.setText(findTestObject('LoginPage/input__password'), findTestData('credential (1)').getValue(2, 6))

WebUI.click(findTestObject('LoginPage/button_login'))

if (WebUI.verifyElementPresent(findTestObject('LoginPage/alert_message_container'), 1, FailureHandling.OPTIONAL)) {
    alert_message = WebUI.getText(findTestObject('LoginPage/alert_message_container'))

    if ((((alert_message == alert_user_required) || (alert_message == alert_password_required)) || (alert_message == alert_invalid_credential)) || 
    alert_locked_out_user) {
        assert false
    } else {
        assert false
    }
} else if (WebUI.verifyElementPresent(findTestObject('HomePage/header_product'), 1, FailureHandling.OPTIONAL)) {
    assert true
} else {
    assert false
}

